#code to connect Impala and iPython Notebooks

# Import all required packages
import pandas as pd
from datetime import datetime
from getpass import getuser as get_user
from getpass import getpass as get_password
from os import environ as enviroment_vars
from os import setpgrp as set_process_group
from shlex import split as cmd_split
from subprocess import Popen, PIPE, STDOUT
import pyodbc

###########################################################
# This file:

###########################################################

### Functions included to handle Kerberos ticket/Hadoop access (user must have keytab file created). ###

def authenticate_kerberos(force=False):
    # get username
    username = get_user()
    # get kerberos realm
    krb_realm = "MILLER.LOCAL"
    
    # create 'klist" command
    cmd = "klist"
    # create linux command process
    process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
    # wait until done, get result
    result = process.communicate(None)[0].decode("utf-8") 
    
    # check that we don't already have a valid ticket
    found_username = False
    expire_date = None
    for line in result.split("\n"):
        if username in line:
            found_username = True
            # update realm if different
            krb_realm = line.split("@")[-1]
        line = line.split()
        if len(line) == 5:
            try:
                expire_date = datetime.strptime(line[2] + " " + line[3],'%m/%d/%Y %H:%M:%S')
            except (ValueError) as e:
                pass
    
    if (found_username) and (expire_date != None) and (expire_date > datetime.today()) and (not force):
        return True
    else:
        # create 'kdestory" command
        cmd = "kdestroy"
        # create linux command process
        process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
        # wait until done, get result
        result = process.communicate(None)[0].decode("utf-8") 

        # create "kinit" command
        cmd = "kinit " + username + "@" + krb_realm
        # create linux command process
        process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
        # wait until done, get result
        result = process.communicate(get_password("Password for " + username + "@" + krb_realm + ": ").encode('utf-8'))[0].decode("utf-8").strip()
        # if result
        if result.strip() != "Password for " + username + "@" + krb_realm + ":":
            print("Failed to obtained Kerberos Ticket.")
            # result
            print(result)
            return False
        # ticket got successfully
        else:
            print("Successfully obtained Kerberos Ticket.\n")
            return True

### Run Kerberos access functions, then run Hadoop query to get data. ###
success = authenticate_kerberos(force=False)


# connect to Impala
cnxn = pyodbc.connect("DSN=Impala", autocommit=True)
cursor = cnxn.cursor()

# Query Function
def impala_select(cnxn,query,show=True,sort_col=None):
    data = pd.read_sql(query,cnxn)
    '''
    if show:
        if sort_col:
            print(data.sort_values(sort_col))
        else:
            print(data)
    '''
    return data
