#####
#
# JupyterHub Example HDFS Notebook for PYTHON 3
# 
# Includes sample code for users to familiarize themselves with connecting to HDFS.
#
# Version 0.5 - 2020/08/04
#
# Written by: Zachary Williams
#
######

from datetime import datetime, timedelta
from getpass import getuser as get_user
from getpass import getpass as get_password
from hdfs.ext.kerberos import KerberosClient
from os import environ as enviroment_vars
from os import setpgrp as set_process_group
from shlex import split as cmd_split
from subprocess import Popen, PIPE, STDOUT
import pyodbc
import pandas as pd

##################################################
#
#    name:   authenticate_kerberos
#    desc:   function that checks if user has a non-expired Kerberos Ticket.
#            If ticket does not exist, or it is expired, will ask for password to
#            generate a new Kerberos Ticket. 
#            Optionally can pass "force=True" to generate a new Ticket even if existing is not expired.
#            Optionally can pass "keytab=True" to use keytab for authentication instead of typing password.
#
#            *** You should include this at beginning of every script that connects to the Hadoop Cluster! ***
#
#    args:
#            force  - (Optional) Force re-creation of Kerberos Ticket.
#            keytab - (Optional) Use Keytab for authentication instead of password.
#                                This assumes that the keytab file is in the user's  
#                                home directory with the name: <username>.keytab.
#                          
#    returns:
#            boolean - True if ticket successfully gotten, false otherwise 
#
#################################################
def authenticate_kerberos(force=False,keytab=False):
    # get username
    username = get_user()
    # get kerberos realm
    krb_realm = "MILLER.LOCAL"
    
    # create 'klist" command
    cmd = "klist"
    # create linux command process
    process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
    # wait until done, get result
    result = process.communicate(None)[0].decode("utf-8") 
    
    # check that we don't already have a valid ticket
    found_username = False
    expire_date = None
    for line in result.split("\n"):
        if username in line:
            found_username = True
            # update realm if different
            krb_realm = line.split("@")[-1]
        line = line.split()
        if len(line) == 5:
            try:
                expire_date = datetime.strptime(line[2] + " " + line[3],'%m/%d/%Y %H:%M:%S')
            except (ValueError) as e:
                pass
    
    if (found_username) and (expire_date != None) and (expire_date > datetime.today()) and (not force):
        return True
    else:
        # create 'kdestory" command
        cmd = "kdestroy"
        # create linux command process
        process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
        # wait until done, get result
        result = process.communicate(None)[0].decode("utf-8") 

        if keytab:
            # create "kinit" command
            cmd = "kinit -kt "+ enviroment_vars["HOME"] + "/" + username + ".keytab " + username + "@" + krb_realm
            # create linux command process
            process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
            # wait until done, get result
            result = process.communicate(None)[0].decode("utf-8").strip()
            # if result
            if result.strip():
                print("Failed to obtained Kerberos Ticket via Keytab.")
                # result
                print(result)
                return False
            # ticket got successfully
            else:
                print("Successfully obtained Kerberos Ticket via Keytab.\n")
                return True
        else:
            # create "kinit" command
            cmd = "kinit " + username + "@" + krb_realm
            # create linux command process
            process = Popen(cmd_split(cmd),stdout=PIPE,stdin=PIPE, stderr=STDOUT,preexec_fn=set_process_group)
            # wait until done, get result
            result = process.communicate(get_password("Password for " + username + "@" + krb_realm + ": ").encode('utf-8'))[0].decode("utf-8").strip()
        
            # if result
            if result.strip() != "Password for " + username + "@" + krb_realm + ":":
                print("Failed to obtained Kerberos Ticket via Password.")
                # result
                print(result)
                return False
            # ticket got successfully
            else:
                print("Successfully obtained Kerberos Ticket via Password.\n")
                return True

###### Write CSV to HDFS Example:
def WriteCSVToHDFS(data_frame,filepath,index=False,header=False):
    host="ip-10-35-20-187.ec2.internal"
    port=14000
    hadoop_filesystem = KerberosClient("http://" + host + ":" + str(port))
    with hadoop_filesystem.write(filepath,overwrite=True,encoding="utf-8") as writer:
        data_frame.to_csv(writer,index=index,header=header)

###### Upload to HDFS Example:
def UploadToHDFS(local_filepath,hdfs_filepath):
    host="ip-10-35-20-187.ec2.internal"
    port=14000
    hadoop_filesystem = KerberosClient("http://" + host + ":" + str(port))
    hadoop_filesystem.upload(hdfs_filepath,local_filepath,n_threads=-1,overwrite=True)

###### Download from HDFS Example:
def DownloadFromHDFS(hdfs_filepath,local_filepath):
    host="ip-10-35-20-187.ec2.internal"
    port=14000
    hadoop_filesystem = KerberosClient("http://" + host + ":" + str(port))
    hadoop_filesystem.download(hdfs_filepath,local_filepath,n_threads=-1)


##################################################
#
#    name:    human_readable_size
#    desc:    function that converts bytes to human readable sizes (KiB, MiB, GiB, TiB, etc.)
#    args:
#            num_bytes        - number of bytes
#    returns:
#            ZZZ.YYYYY XiB    - bytes with human readable unit 
#
#################################################
def human_readable_size(num_bytes):
    # go through all units
    for unit in ['B','KiB','MiB','GiB','TiB','PiB','EiB','ZiB']:
        # if number is less than 1024
        if abs(num_bytes) < 1024.0:
            # return
            return "%3.2f %s" % (num_bytes, unit)
        # reduce the number by 1024 and go to next unit
        num_bytes /= 1024.0
    # return yobibytes
    return "%3.2f %s" % (num_bytes, 'YiB')

###### List a Directory on HDFS Example:
def ListDirOnHDFS(hdfs_filepath):
    perm_map = {
        "0": "---",
        "1": "--x",
        "2": "-w-",
        "3": "-wx",
        "4": "r--",
        "5": "r-x",
        "6": "rw-",
        "7": "rwx"
        }
    host="ip-10-35-20-187.ec2.internal"
    port=14000
    hadoop_filesystem = KerberosClient("http://" + host + ":" + str(port))
    files = hadoop_filesystem.list(hdfs_filepath,status=True)
    lines = []
    lines.append(["Type","Permissions","Owner","Group","Size","Mod Date","Name"])
    line_idx = 1
    for file in files:
        lines.append([])
        lines[line_idx].append(file[1]["type"])
        perms = ""
        for perm in file[1]["permission"]:
            perms += perm_map[perm]
        if file[1]['aclBit']:
            perms += "+"
        lines[line_idx].append(perms)
        lines[line_idx].append(file[1]["owner"])
        lines[line_idx].append(file[1]["group"])
        lines[line_idx].append(human_readable_size(file[1]["length"]))
        lines[line_idx].append(datetime.fromtimestamp(file[1]["modificationTime"]/1000).strftime("%Y-%m-%d %H:%M:%S"))
        lines[line_idx].append(file[0])
        line_idx += 1
    
    long_line = [0,0,0,0,0,0,0,0]
    for line in lines:
        cell_idx = 0
        for cell in line:
            if len(cell) > long_line[cell_idx]:
                long_line[cell_idx] = len(cell)
            cell_idx += 1
    
    for line in lines:
        cell_idx = 0
        new_line = ""
        for cell in line:
            new_line += cell + " "*(long_line[cell_idx] - len(cell)) + "  "
            cell_idx+=1
        print(new_line)

#### IMPALA CONNECT ####
### Run Kerberos access functions, then run Hadoop query to get data. ###
success = authenticate_kerberos(force=False,keytab=False)

# connect to Impala
cnxn = pyodbc.connect("DSN=Impala", autocommit=True)
cursor = cnxn.cursor()

# Query Function
def impala_select(cnxn,query,show=True,sort_col=None):
    data = pd.read_sql(query,cnxn)
    '''
    if show:
        if sort_col:
            print(data.sort_values(sort_col))
        else:
            print(data)
    '''
    return data


'''   
###### MAIN
if success:
    # create sample Pandas DF
    data = [['tom', 10], ['nick', 15], ['juli', 14]] 
    df = pd.DataFrame(data, columns = ['Name', 'Age']) 
    
    # Write DF directly to HDFS as CSV (Parquet not yet supported)
    WriteCSVToHDFS(df,'/user/'+ get_user() + '/csv_test/test.csv',False,False)
    print("CSV Written to HDFS Successfully:")
    # verify upload
    ListDirOnHDFS('/user/'+ get_user() + '/csv_test/')
    print("\n")
    
    # Write DF to local file system and then upload to HDFS
    df.to_parquet(enviroment_vars["HOME"] + "/test.parquet.snappy", index=False,compression="snappy")
    UploadToHDFS(enviroment_vars["HOME"] + "/test.parquet.snappy",'/user/'+ get_user() + '/parquet_test/test.parquet.snappy')
    print("Parquet Written to HDFS Successfully:")
    # verify upload
    ListDirOnHDFS('/user/'+ get_user() + '/parquet_test/')
'''
